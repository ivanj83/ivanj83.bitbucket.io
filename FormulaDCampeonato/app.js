console.log("agregado");
  // Your web app's Firebase configuration
  /*
  var firebaseConfig = {
    apiKey: "AIzaSyAhSe31PHZJaYAuC0eG0IwoW8PvaAIbBb0",
    authDomain: "formulad-torneo-1.firebaseapp.com",
    databaseURL: "https://formulad-torneo-1.firebaseio.com",
    projectId: "formulad-torneo-1",
    storageBucket: "formulad-torneo-1.appspot.com",
    messagingSenderId: "190920174309",
    appId: "1:190920174309:web:73e91679d6e66eca9ac20c"
  };
*/
  var firebaseConfig = {
    apiKey: "AIzaSyAhSe31PHZJaYAuC0eG0IwoW8PvaAIbBb0",
    authDomain: "formulad-torneo-1.firebaseapp.com",
    projectId: "formulad-torneo-1"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  var db = firebase.firestore();

  //leer colecciones
  var tabla = document.getElementById("tablaParticipantes");
  var contador = 0;
  /*
  db.collection('participantes').get()
  .then((snapshot) => {
    tabla.innerHTML=""; 
    contador = 0;
    snapshot.forEach((doc) => {
        contador++;
        console.log(doc.id, '=>', doc.data());
        tabla.innerHTML += "<tr><th scope='row'>"+contador+"</th><td>"+doc.data().nombre+"</td><td>"+doc.data().ganadas+"</td><td>"+doc.data().puntos+"</td></tr>";
    });
  })
  .catch((err) => {
    console.log('Error getting documents', err);
  });
  */
 db.collection('participantes').orderBy('posicion').onSnapshot((snapshot) => {
   tabla.innerHTML=""; 
   contador = 0;
   snapshot.forEach((doc) => {
       contador++;
       console.log(doc.id, '=>', doc.data());
       tabla.innerHTML += "<tr><th scope='row'>"+contador+"</th><td>"+doc.data().nombre+"</td><td><strong>"+doc.data().puntos+"</strong></td><td>"+doc.data().ganadas+"</td></tr>";
   });
 });
  console.log("fin");